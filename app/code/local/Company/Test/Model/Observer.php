<?php
class Company_Test_Model_Observer
{
    /**
     * Used topmenu because toplinks is deprecated from V1.4. see Mage_Page_Block_Html_Toplinks
     *
     * @param Varien_Event_Observer $observer
     */
    public function addurltopmenu(Varien_Event_Observer $observer){
        $name = 'News';
        $url = Mage::getUrl('test/read/allnews');

        $menu = $observer->getMenu();
        $tree = $menu->getTree();


        $node = new Varien_Data_Tree_Node(array(
            'name'   => $name,
            'id'     => $name,
            'url'    => $url,
        ), 'id', $tree, $menu);

        $menu->addChild($node);
    }
}
