<?php
$installer = $this;

$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS `{$this->getTable('news')}`;
CREATE TABLE `{$this->getTable('news')}` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `author` varchar(255) NOT NULL default '',
  `category` varchar(255) NOT NULL default '',
  `status` 	tinyint(1) NOT NULL default '0',
  `date_created` timestamp NOT NULL default current_timestamp,
  `date_modified` timestamp NOT NULL default 0,
  `content` text NOT NULL default '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 ");
$installer->run("INSERT INTO `{$this->getTable('news')}` (
`id` ,
`title` ,
`author`,
`category`,
`status` ,
`date_created` ,
`date_modified` ,
`content`
)
VALUES
(NULL , 'City have new car!', 'Pushkin', 'car', '1', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'The city have new car. The city have new new car. The city have new new car.'),
(NULL , 'New android!', 'Petrov', 'android', '1', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'New android. New android. New android.'),
(NULL , 'City news 1', 'Pupkin', 'city', '1', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'The city have new car. The city have new new car. The city have new new car.The city have new new car. The city have new new car.The city have new new car. The city have new new car.')
");
//todo only for test case. Remove in future
sleep(2);
$installer->run("INSERT INTO `{$this->getTable('news')}` (
`id` ,
`title` ,
`author`,
`category`,
`status` ,
`date_created` ,
`date_modified` ,
`content`
)
VALUES
(NULL , 'Android News', 'Voevoda', 'android', '1', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Android News. Android News. Android News. Android News.'),
(NULL , 'World News', 'Kirkorov', 'world', '1', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'World News. World News. World News. World News. World News.')
");

$installer->endSetup();
