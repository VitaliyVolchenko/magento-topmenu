<?php

class Company_Test_Block_Adminhtml_News_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('test_form', array('legend'=>Mage::helper('test')->__('News information')));

        $fieldset->addField('title', 'text', array(
            'name'      => 'title',
            'label'     => Mage::helper('test')->__('Title'),
            'title'     => Mage::helper('test')->__('Title'),
            'required'  => true,
        ));

        $fieldset->addField('author', 'text', array(
            'name'      => 'author',
            'label'     => Mage::helper('test')->__('Author'),
            'title'     => Mage::helper('test')->__('Author'),
            'required'  => true,
        ));

        $fieldset->addField('category', 'text', array(
            'name'      => 'category',
            'label'     => Mage::helper('test')->__('Category'),
            'title'     => Mage::helper('test')->__('Category'),
            'required'  => true,
        ));

        $fieldset->addField('status', 'select', array(
            'label'     => Mage::helper('test')->__('Status'),
            'name'      => 'status',
            'values'    => array(
                array(
                    'value'     => 1,
                    'label'     => Mage::helper('test')->__('Enabled'),
                ),

                array(
                    'value'     => 2,
                    'label'     => Mage::helper('test')->__('Disabled'),
                ),
            ),
        ));

        $fieldset->addField('content', 'textarea', array(
            'name'      => 'content',
            'label'     => Mage::helper('test')->__('Content'),
            'title'     => Mage::helper('test')->__('Content'),
            'required'  => true,
            'style' => 'height:300px;width:700px',
        ));


        if ( Mage::getSingleton('adminhtml/session')->getTestData() )
        {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getTestData());
            Mage::getSingleton('adminhtml/session')->setTestData(null);
        } elseif ( Mage::registry('test_data') ) {
            $form->setValues(Mage::registry('test_data')->getData());
        }
        return parent::_prepareForm();
    }
}
