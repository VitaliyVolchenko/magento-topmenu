<?php

class Company_Test_Block_Adminhtml_News_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('test_news_grid');
        $this->setUseAjax(true);
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('test/news')->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }


    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'    => Mage::helper('test')->__('ID'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'id',
        ));

        $this->addColumn('title', array(
            'header'    => Mage::helper('test')->__('Title'),
            'align'     =>'left',
            'index'     => 'title',
        ));

        $this->addColumn('author', array(
            'header'    => Mage::helper('test')->__('Author'),
            'align'     =>'left',
            'index'     => 'author',
        ));

        $this->addColumn('category', array(
            'header'    => Mage::helper('test')->__('Category'),
            'align'     =>'left',
            'index'     => 'category',
        ));
        $this->addColumn('status', array(
            'header'    => Mage::helper('test')->__('Status'),
            'align'     => 'left',
            'width'     => '80px',
            'index'     => 'status',
            'type'      => 'options',
            'options'   => array(
                1 => 'Enabled',
                2 => 'Disabled',
            ),
        ));

        $this->addColumn('date_created', array(
            'header'    => Mage::helper('test')->__('Date Created'),
            'type'      => 'datetime',
            'index'     => 'date_created',
        ));

        $this->addColumn('date_modified', array(
            'header'    => Mage::helper('test')->__('Date Modified'),
            'type'      => 'datetime',
            'index'     => 'date_modified',
        ));

        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('test')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('test')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
            ));
        return parent::_prepareColumns();

    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('test');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'    => Mage::helper('test')->__('Delete'),
            'url'      => $this->getUrl('*/*/massDelete'),
            'confirm'  => Mage::helper('test')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('test/news_status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
            'label'=> Mage::helper('test')->__('Change status'),
            'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('test')->__('Status'),
                    'values' => $statuses
                )
            )
        ));
        return $this;
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}
