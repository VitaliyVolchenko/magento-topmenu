<?php

class Company_Test_Block_Adminhtml_News extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'test';
        $this->_controller = 'adminhtml_news';
        $this->_headerText = Mage::helper('test')->__('News Manager');
        $this->_addButtonLabel = Mage::helper('test')->__('Add News');
        parent::__construct();
    }
}
