<?php
class Company_Test_Block_Allnews extends Mage_Core_Block_Template
{
    public function getAllNews()
    {
        $page = $this->getRequest()->getParam('page');

        if (is_null($page)) {
            $page = 1;
        }

        $collection = Mage::getModel('test/news')->getCollection();
        $loadedAllNews = $collection->setOrder('date_created')
            ->setPageSize($this->getCountOnThePage())
            ->setCurPage($page)
            ->addFieldToFilter('status', true)
            ->load();
        return $loadedAllNews;
    }

    public function isDateCreatedEnabled()
    {
        return Mage::getStoreConfigFlag('test_options/messages/show_date');
    }

    public function getCountOnThePage()
    {
        $pagination = Mage::getStoreConfig('test_options/messages/pagenation');
        return $pagination;
    }
}
