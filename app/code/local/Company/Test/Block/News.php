<?php
class Company_Test_Block_News extends Mage_Core_Block_Template
{
    protected $_model = null;

    public function getModel()
    {
        if (is_null($this->_model)) {
            $this->_model = Mage::getModel('test/news')->load($this->getRequest()->getParam('id'));
        }
        return $this->_model;
    }

    public function getNewsTitle()
    {
        return $this->getModel()->getTitle();
    }

    public function getNewsContent()
    {
        $countSymbols = Mage::getStoreConfig('test_options/messages/symbol_count');
        return substr($this->getModel()->getContent(), 0, $countSymbols);
    }

    public function getNewsDateCreated()
    {
        return $this->getModel()->getDateCreated();
    }

    public function isDateCreatedEnabled()
    {
        return Mage::getStoreConfigFlag('test_options/messages/show_date');
    }

}